#!/usr/local/bin/python3
def buildIndexFile():
    class_dir = './se'
    jar_dir = '../libraries/*'
    search_engine = 'searchEngine.SearchEngine'

    # change working dir
    import os
    import platform
    os.chdir(class_dir)

    javaArgs = "http://www.hkbu.edu.hk/eng/main/index.jsp 100 10"
    # invoke java search engine
    #print('java -cp \".:' + jar_dir + '\" ' + search_engine + ' ' + query)
    if(platform.system() != "Windows"):
        os.system('java -cp \".:' + jar_dir + '\" ' + search_engine + ' ' + javaArgs)
    else:
        os.system('java -cp \".;' + jar_dir + '\" ' + search_engine + ' ' + javaArgs)

buildIndexFile()
