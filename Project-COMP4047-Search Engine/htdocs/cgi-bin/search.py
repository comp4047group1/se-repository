#!/usr/local/bin/python3
class_dir = './se'
jar_dir = '../libraries/*'
search_engine = 'searchEngine.SearchServer'

# enable cgi traceback manager
import cgitb
cgitb.enable()

# get form data
import cgi
form = cgi.FieldStorage()
if 'query' in form:
    query = form['query'].value
else:
    query = ''

# change working dir
import os
import platform
os.chdir(class_dir)

# invoke java search engine
#print('java -cp \".:' + jar_dir + '\" ' + search_engine + ' ' + query)
if(platform.system() != "Windows"):
    os.system('java -cp \".:' + jar_dir + '\" ' + search_engine + ' ' + query)
else:
    os.system('java -cp \".;' + jar_dir + '\" ' + search_engine + ' ' + query)
