import shutil
import os
import sys

def updateIndexFile():
    ide_path = "./../se/out.txt"
    term_path = "./se/out.txt"

    if os.path.isfile(term_path):
        if os.path.isfile(ide_path):
            print("Index file found both in IDE root path and server path.")
            ide_time = os.stat(ide_path).st_mtime
            term_time = os.stat(term_path).st_mtime
            if ide_time > term_time:
                print("The index file in IDE path seems newer. The file in server path will be replaced")
                shutil.copyfile(ide_path, term_path)
            else:
                print("The index file in server path seems newer. Nothing will be changed.")
        else:
            print("Index file already exists in the server path rather than IDE root path. Nothing will be changed.")
    else:
        if os.path.isfile(ide_path):
            print("Index file found in IDE root path. It will be copied to server path")
            shutil.copyfile(ide_path, term_path)
        else:
            print("No index file found. Please run buildIndex.py first.")
            sys.exit(0)

#updateIndexFile()