package searchEngine;

import org.jsoup.select.Elements;

import java.util.LinkedList;

/**
 * Created by WEI Wenzhou on 27/9/2016.
 */
public class Crawler {

    /**
     * Crawl and filter each html file
     * Pass filtered content to HTMLProcessor
     */
    static LinkedList<String> urlPool = new LinkedList<>();
    static LinkedList<String> processedUrlPool = new LinkedList<>();
    private boolean isExistInUrlPool = false;
    private boolean isExistInProcessedUrlPool = false;
    private boolean isLinksEmpty = false;
    private boolean isDropable = false;
    int Y = -1;
    int X = -1;
    private Elements links;
    private String redirectedUrl = "";
    private String urlForKeyword;
    private String title;
    private String plainText;
    private int totalKeywordNum;

    /**
     * Constructor
     * @param url The initial URL input
     * @param X The capacity of URL Pool
     * @param Y The capacity of Processed URL Pool
     */
    public Crawler(String url, int Y, int X) {
        urlPool.add(url); //Add initial URL into URL Pool
        System.out.println("Initial URL Added: " + url);
        redirectedUrl = url;
        this.Y = Y;
        this.X = X;
    }

    /**
     * Crawling Method
     * Start to crawl
     */
    public void start() {

        while (processedUrlPool.size() < Y) {
            HttpConnector httpConnector = new HttpConnector(); //Create a new HTTP Connection

            /**
             * Check if the retrieved URL is required to be redirected.
             */
            if (!isLinksEmpty) {
                httpConnector.get(urlPool.getFirst());
            } else {
                if (!isDropable) {
                    httpConnector.get(redirectedUrl);
                } else {
                    dropUrl(httpConnector);
                }
            }

            /**
             * Get final links and their URLs.
             */
            links = httpConnector.getLinks();
            redirectedUrl = httpConnector.getRedirectedUrl();
            urlForKeyword = redirectedUrl;
            title = httpConnector.getTitle();
            plainText = httpConnector.getPlainText();

            isLinksEmpty = httpConnector.getIsLinksEmpty(); //Check if a URL has no links.
            isDropable = httpConnector.getIsDropable(); //Check if a URL cannot be handled and if so, then drop it.

            /**
             * Send to Keyword Processor.
             */
            if (!isLinksEmpty) {
                System.out.println();
                SearchEngine.bridge(urlForKeyword, links, title, plainText);
            }

            if (links != null) {
                updateUrlPool(); // Update URL Pool
            } else {
                dropUrl(httpConnector);
            }
        }
        System.out.println();
        System.out.println("Crawling succeeded!");
        System.out.println("URLs in Processed URL Pool: " + processedUrlPool.size());
    }

    /**
     * Update URL Pool Method
     */
    private void updateUrlPool() {

        /**
         * Add retrieved URL into Processed URL Pool.
         */
        if(!isLinksEmpty) {
            if (urlPool.size() > 0) {
                processedUrlPool.add(urlPool.getFirst());
                urlPool.removeFirst();
                System.out.println("URL Pool Deleted: " + urlPool);
            } else {
                System.out.println("All new URLs extracted from URLs in URL Pool cannot be added, so crawling is terminated.");
                System.out.println("URLs in Processed URL Pool: " + processedUrlPool.size());
                System.exit(2);
            }
        }

        /**
         * Filter each links.
         */
        for (int i = 0; i < links.size(); i++) {

            String absHref = links.get(i).attr("abs:href"); //Get absolute URL.

            /**
             * Compare the link with those in URL Pool
             */
            for (String url: urlPool) {
                if (absHref.equals(url)) {
                    isExistInUrlPool = true;
                    break;
                }
            }

            /**
             * Compare the link with those in Processed URL Pool
             */
            for (String url: processedUrlPool) {
                if (isExistInUrlPool) {
                    break; //Skip if exist in URL Pool
                } else if (absHref.equals(url)) {
                    isExistInProcessedUrlPool = true;
                    break;
                }
            }

            /**
             * Add Selected URL into URL Pool.
             */
            if (!isExistInProcessedUrlPool
                    && !isExistInUrlPool
                    && !absHref.equals("")
                    && !absHref.endsWith(".pdf")
                    && !absHref.endsWith(".doc")
                    && !absHref.endsWith(".png")
                    && !absHref.endsWith(".jpg")
                    && !absHref.contains("@")) {
                /**
                 * Check if the URL Pool is full.
                 */
                if (urlPool.size() < X) {
                    urlPool.add(absHref);
                    System.out.println("URL Pool Added: " + urlPool);
                } else {
                    break;
                }
            }

            /**
             * Restore the booleans.
             */
            isExistInUrlPool = false;
            isExistInProcessedUrlPool = false;
        }
    }

    /**
     * Compulsively drop the URL cannot be handled.
     * @param httpConnector The HTTP Connector to retrieve the given URL
     */
    private void dropUrl(HttpConnector httpConnector) {
        httpConnector.get(urlPool.getFirst());
        urlPool.removeFirst();
        System.out.println("URL Pool Deleted: " + urlPool);
    }
}