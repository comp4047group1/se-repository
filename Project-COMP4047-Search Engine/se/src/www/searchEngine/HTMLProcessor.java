package searchEngine;

import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.LinkedList;

/**
 * Created by PIAO Bingjun on 9/30/2016.
 */
public class HTMLProcessor {
    private String pageURL;
    private String title;
    private LinkedList<String> content = new LinkedList<String>();
    private int positionCounting = 0;

    private static IOProcessor io;
    public static HashSet<String> stopList = null;

    /**
     * @param url The URL to be extracted
     * @param link The HTML lines containing URLs og given URL
     * @param title The title of the given URL
     * @param plainText The plain text in the given URL
     */
    public HTMLProcessor(String url, Elements link, String title, String plainText){
        this.pageURL = url;
        this.title = title;
        if (link == null) {
            System.out.println("HTML Processor: The page is not existing!");
        } else if (link.size() > 0) {
            content = processKeywords(plainText, link);
            System.out.println("HTML Processor: " + this.title + ", " + pageURL + ": " + content.size() + " keywords found.");
        } else {
            System.out.println("HTML Processor: " + pageURL + ": empty page. Dropped.");
        }
    }

    /**
     * Write keywords into Keyword Table Row
     */
    public void writeKeyWords(){
        Keyword[] keywords = new Keyword[content.size()];
        for(int i = 0; i < keywords.length; i++){
            keywords[i] = new Keyword(content.get(i), positionCounting);
            positionCounting++;
        }
        KeywordTableRow row = new KeywordTableRow(title, pageURL, keywords);

        io.writeKeywordTableRow(row);
    }

    /**
     * To split and store keywords from plain text and URLs into a linked list
     * @param plainText The plain text in the given URL
     * @param link The HTML lines containing URLs og given URL
     * @return The linked list containing keywords of each line of link
     */
    private static LinkedList<String> processKeywords(String plainText, Elements link){
        LinkedList<String> keywords = new LinkedList<String>();

        String[] thisTextLine = plainText.toLowerCase().split(" ");
        addWordToKeywords(thisTextLine, keywords);

        int size = link.size();
        for (int i = 0; i < size; i++){
            String[] thisLinkLine = link.get(i).text().toLowerCase().split(" ");

            addWordToKeywords(thisLinkLine, keywords);
        }
        return keywords;
    }

    /**
     * To add the keywords in the linked list into keyword Table Row
     * @param thisLine The String array that containing all the keywords in a certain link
     * @param keywords The linked list containing all the keywords
     */
    private static void addWordToKeywords(String[] thisLine, LinkedList<String> keywords){
        for (int i = 0; i < thisLine.length; i++) {
            String thisWord = applyStopList(thisLine[i]);
            if (thisWord != null) {
                thisWord = thisWord.replaceAll("[^\\w\\s]", "");
                if (!thisWord.isEmpty()) {
                    keywords.add(thisWord);
                }
            }
        }
    }

    /**
     * Filter out the undesirable keywords according to the requirement
     * @param word The word to be detected to be filtered out or not
     * @return The result of the filter
     */
    private static String applyStopList(String word){
        int size = stopList.size();
        for(int i = 0; i < size; i++){
            if(stopList.contains(word)){
                return null;
            }
        }
        return word;
    }

    /**
     * Create an IO Processor
     * @param ioProcessor The processor to write the file into out.txt
     */
    public static void setIOProcessor(IOProcessor ioProcessor){
        io = ioProcessor;
        stopList = io.getStopList();
    }

    /**
     * Getter of the content containing keywords.
     * @return Content
     */
    public LinkedList<String> getContent() { return content; }
}
