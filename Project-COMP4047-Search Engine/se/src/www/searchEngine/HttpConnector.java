package searchEngine;

import java.io.IOException;
import java.net.ConnectException;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
/**
 * Created by WEI Wenzhou on 27/9/2016.
 */
public class HttpConnector {

    /**
     * Retrieve html file of given url.
     */

    private Elements links;
    private Elements redirectionLinks;
    private Document doc;
    private boolean isLinksEmpty = false;
    private boolean isDropable = false;
    private String absRedirectedUrl;
    private String url;
    private String title;
    private String plainText;

    /**
     * Get links and their final URL.
     * @param url The URL to be extracted
     */
    public void get(String url) {
        try {
            this.url = url;
            Connection connection = Jsoup.connect(url)
                    .ignoreContentType(true) //Ignore content type
                    .ignoreHttpErrors(true) //Ignore HTTP Errors
                    .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21") //Forbid mobile access
                    .validateTLSCertificates(false) //Validate Certification Authentication
                    .timeout(10000); //Finally, Create HTTP Connection
            try {
                Connection.Response response = connection.execute(); //Execute the connection
                doc = connection.get(); //Parse the HTML files

                /**
                 * Drop the URL if still disconnected
                 */
                if (response.statusCode() != 200) {
                    isDropable = true;
                }
            } catch (ConnectException c) {
                isDropable = true;
                System.out.println("Connect Exception occurs. The URL will be dropped!");
            }

            links = doc.select("a"); //Select links beginning with "a" -- URLs
            title = doc.select("title").text(); //Select links beginning with "a" -- title
            plainText = doc.select("p").text(); //Select links beginning with "p" -- plain text
            absRedirectedUrl = url;

            /**
             * Check if the URL is required to be redirected
             */
            if (links.size() == 0) {
                if (!isDropable) {
                    redirectUrl();
                } else {
                    isLinksEmpty = true;
                }
            }
        } catch (IOException e) {
            isDropable = true;
            System.out.println("IO Exception occurs. The URL will be dropped!");
            System.err.flush(); //Handle IO exception by flushing "err buffer stream"
        }
    }

    /**
     * Get redirected URL Method.
     */
    private void redirectUrl() {
        isLinksEmpty = true;
        redirectionLinks = doc.select("meta"); //Select links beginning with "meta"

        /**
         * Drop the URL if there is no "mata" tag.
         */
        if (redirectionLinks.size() == 0) {
            isDropable = true;
        }

        /**
         * Find the new URL to be redirected.
         */
        for (Element redirectedLink: redirectionLinks) {
            if (redirectedLink.attr("http-equiv").toLowerCase().equals("refresh")) { //Determine whether the URL can be redirected.
                String redirectedUrl = redirectedLink.attr("content"); //Try to find the redirected URL.
                if (redirectedUrl.contains("http")) { //Handle the URL to be redirected with HTTP.
                    absRedirectedUrl = redirectedUrl.substring(redirectedUrl.indexOf("http"));
                } else { //Handle the URL to be redirected with only postfix.
                    if (url.endsWith("/")) {
                        if (redirectedUrl.contains("=/") || redirectedUrl.contains("../")) {
                            absRedirectedUrl = redirectedUrl.substring(redirectedUrl.indexOf("/")+1);
                        } else {
                            absRedirectedUrl = redirectedUrl.substring(redirectedUrl.indexOf("=")+1);
                        }
                    } else {
                        if (redirectedUrl.contains("=/") || redirectedUrl.contains("../")) {
                            absRedirectedUrl = redirectedUrl.substring(redirectedUrl.indexOf("/"));
                        } else {
                            absRedirectedUrl = "/" + redirectedUrl.substring(redirectedUrl.indexOf("=")+1);
                        }
                    }

                    /**
                     * Get rid of duplication
                     */
                    if (url.contains(absRedirectedUrl)) {
                        absRedirectedUrl = url;
                        break;
                    } else {
                        absRedirectedUrl = url + absRedirectedUrl;
                    }
                }
                System.out.println("Redirecting to " + absRedirectedUrl);
                break;
            }
        }

        /**
         * The URL cannot be redirected, then we drop it.
         */
        if (absRedirectedUrl.equals(url)) {
            isDropable = true;
            System.out.println("Redirection Fail! Drop " + absRedirectedUrl);
        }
    }

    /**
     * Getter of links
     * @return The HTML lines containing URLs
     */
    public Elements getLinks() {
        return links;
    }

    /**
     * Getter of absolute redirected URL
     * @return The redirected URL is any
     */
    public String getRedirectedUrl() {
        return absRedirectedUrl;
    }

    /**
     * Getter of if links are empty
     * @return If the links are empty or not
     */
    public boolean getIsLinksEmpty() {
        return isLinksEmpty;
    }

    /**
     * Getter of if links can be dropped
     * @return If the URL can be dropped or not
     */
    public boolean getIsDropable() {
        return isDropable;
    }

    /**
     * Getter of the title of the URL
     * @return The title of the URL
     */
    public String getTitle() {
        return title;
    }

    /**
     * Getter of the plain text in the URL
     * @return The plain text in the URL
     */
    public String getPlainText() {
        return plainText;
    }
}
