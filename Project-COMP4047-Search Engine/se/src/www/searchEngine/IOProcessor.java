package searchEngine;

import java.io.*;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Created by PIAO Bingjun on 9/29/2016.
 */
public class IOProcessor {

    static String defaultOut = "out.txt";
    static String defaultFilter = "filter.txt";

    File data;
    //BufferedReader reader;
    BufferedWriter writer;

    /**
     * Default Constructor using default path.
     * The default paht is set to be out/production/se/out.txt
     */
    public IOProcessor(){
        this(defaultOut);
    }

    /**
     * IOProcessor is used to create index file and get stop list
     * @param outPath The path for index file
     */
    public IOProcessor(String outPath){
        try{
            data = new File(outPath);

            if (!data.exists()) {
                data.createNewFile();
                System.out.println(outPath + " not found. New file created.");
            } else {
                clearFile(data);
                System.out.println(outPath + " found, cleared for new data.");
            }

            //reader = new BufferedReader(new FileReader(data));
            FileOutputStream fileOutputStream = new FileOutputStream(data, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF8");
            writer = new BufferedWriter(outputStreamWriter);
        } catch (IOException e){
            e.printStackTrace();
        }
    }


    public void writeKeywordTableRow(KeywordTableRow keywordTableRow){
        try {
            writer.write(keywordTableRow.createJsonTableRow());
            writer.flush();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Clear the existing file
     * @param data To file to clear
     */
    private static void clearFile(File data){
        try{
            FileWriter cleaner = new FileWriter(data, false);
            cleaner.write("");
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    /**
     * Get the stop list from default stop list path.
     * The default path is out/production/se/filter.txt
     * @return The stop list in linked list
     */
    public HashSet<String> getStopList(){
        return getStopList(defaultFilter);
    }

    /**
     * Get stop list from the path
     * @param path The path to stop list
     * @return The stop list in linked list
     */
    public HashSet<String> getStopList(String path){
        File filter = new File(path);
        HashSet<String> stopList = new HashSet<String>();

        try {
            if (!filter.exists()){
                System.out.println("Filter file not Found!");
                return null;
            } else {
                System.out.println(path + " as filter file found.");
                BufferedReader stopListReader = new BufferedReader(new FileReader(filter));
                String line;
                while ((line=stopListReader.readLine()) != null) {
                    stopList.add(line);
                }

                stopListReader.close();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        System.out.println("Totally " + stopList.size() + " filter words read.");
        return stopList;
    }

    /**
     * Close the writer buffer.
     * Should be called after the instance is useless
     */
    public void closeBuffer(){
        try {
            //reader.close();
            writer.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }


}
