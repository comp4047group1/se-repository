package searchEngine;

import javax.swing.text.html.HTML;

/**
 * Created by PIAO Bingjun on 10/17/2016.
 */
public class Keyword {
    int positionNumber;
    private String keyword;
    private String url;
    private String title;

    /**
     * Create Keyword instance when writing index file. url and title is not needed.
     * @param keyword keyword
     * @param positionNumber the position of the keyword.
     */
    public Keyword(String keyword, int positionNumber)
    {
        this.keyword = keyword;
        this.positionNumber = positionNumber;
        url = null;
        title = null;
    }

    /**
     * Create keyword instance from json index file. url and title are used for display search result.
     * @param title title of the page
     * @param url url of the page
     * @param keyword keyword
     * @param positionNumber the position of the keyword
     */
    public Keyword(String title, String url, String keyword, int positionNumber){
        this.title = title;
        this.url = url;
        this.keyword = keyword;
        this.positionNumber = positionNumber;
    }

    /**
     * Convert a keyword instance to json array format.
     * @return
     */
    public String toJsonArray(){
        return "\"" + keyword + "\",\"" + positionNumber + "\"";
    }


    @Override
    public String toString() {

        return  "<section>" +
                "<a href=" + '\"' + this.url +'\"' + " style=\"font-size: 20px\"" +">"
                + this.title //+ "_" + this.getPositionNum()
                + "</a>" +
                "<p>" + url + "</p>" +
                "</section>" +
                "<br />";
    }

    /**
     * Get the position number
     * @return Position number
     */
    public int getPositionNum() {
        return positionNumber;
    }

    public String getKeyword(){
        return keyword;
    }

}
