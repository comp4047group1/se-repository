package searchEngine;

/**
 * Created by WEI Wenzhou on 9/30/2016.
 */
public class KeywordTableRow {
    private String url;
    private String title;
    private Keyword[] keywords;

    /**
     * It is used to exchange date between HTMLProcessor and IOProcessor
     * @param url The url of the HTML page
     * @param title The tile of this page
     * @param keywords Array of keywords for this page
     */
    public KeywordTableRow(String title, String url, Keyword[] keywords) {
        //this.keyword = keyword;
        this.title = title;
        this.url = url;
        //this.positionNum = positionNum;
        this.keywords = keywords;
    }


    /**

     * Convert the table row from String[]
     * @param json The String array.
     */

    public KeywordTableRow(String[] json) {
        this.title = json[0];
        this.url = json[1];
        Keyword[] keywords = new Keyword[(json.length - 2)/2];
        for(int i = 0; i < keywords.length; i++){
            keywords[i] = new Keyword(title, url, json[i * 2 + 2], Integer.parseInt(json[i * 2 + 3]));
        }
        this.keywords = keywords;
    }

    /**
     * Get keywords
     * @return Keyword array
     */
    public Keyword[] getKeywords() {
        return keywords;
    }

    /**
     * get url
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * get title
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Create a json table row.
     * @return The json row in String.
     */
    public String createJsonTableRow(){
        String tableRow = "[";

        tableRow += "\"" + title + "\",";

        tableRow += "\"" + url + "\",";

        //tableRow += "[";
        for(int i = 0; i < keywords.length; i++){
            tableRow += keywords[i].toJsonArray();
            if( i < keywords.length - 1){
                tableRow += ",";
            }
        }

        tableRow += "]";

        tableRow += "\n";
        return tableRow;
    }

    /**
     * Create HTML style table row
     * @return The HTML row
     */

    @Override
    public String toString() {
        String result = "<section>" +
                "<a href=" + '\"' + this.getUrl() +'\"' + ">"
                        + this.getTitle() //+ "_" + this.getPositionNum()
                        + "</a>" +
                "</section>";
        return result;
    }

}
