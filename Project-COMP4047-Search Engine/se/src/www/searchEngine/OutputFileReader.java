package searchEngine;

import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Jason HO on 21/10/2016.
 */
public class OutputFileReader
{
    private Scanner scanner;
    private Gson gson;

    OutputFileReader(String outputName){
        gson = new Gson();
        try
        {
            Path currPath = Paths.get(outputName);

            FileInputStream fileInputStream = new FileInputStream(new File(currPath.toAbsolutePath().toString()));
            scanner = new Scanner(new BufferedInputStream(fileInputStream), "UTF8");
        } catch (FileNotFoundException fne) {
            System.out.println("File cannot be open or it does not exist");
//            fne.printStackTrace();
            System.exit(1);
        }
    }


    Hashtable<String, Hashtable<Integer, HashSet<KeywordTableRow>>> getHashTable()
    {
        Hashtable hashtable = new Hashtable<String, Hashtable<Integer, HashSet<KeywordTableRow>>>();

        while (scanner.hasNextLine()) {
            String[] json = gson.fromJson(scanner.nextLine(), String[].class);
            KeywordTableRow keywordTableRow = new KeywordTableRow(json);
            Keyword[] keywords = keywordTableRow.getKeywords();

            for (int i = 0; i < keywords.length; i++){
                Hashtable posToRows;
                if (hashtable.containsKey(keywords[i].getKeyword())){
                    posToRows = (Hashtable) hashtable.get(keywords[i].getKeyword());
                    HashSet<KeywordTableRow> set;
                    if (posToRows.containsKey(keywords[i].getPositionNum())) {
                        set = (HashSet) posToRows.get(keywords[i].getPositionNum());
                        set.add(keywordTableRow);
                    } else {
                        set = new HashSet<>();
                        set.add(keywordTableRow);
                        posToRows.put(keywords[i].getPositionNum(), set);
                    }
                } else {
                    posToRows = new Hashtable<Integer, HashSet<KeywordTableRow>>();
                    Set set = new HashSet<KeywordTableRow>();
                    set.add(keywordTableRow);
                    posToRows.put(keywords[i].getPositionNum(), set);
                    hashtable.put(keywords[i].getKeyword(), posToRows);
                }
            }
        }

        return hashtable;
    }
}
