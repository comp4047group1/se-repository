package searchEngine;

import java.util.Arrays;
import java.util.HashSet;
import java.nio.charset.*;

/**
 * Created by Jason HO on 21/10/2016.
 */

public class ResponseServiceProvider {
    private String result;
    private String encodeResult;
    private static Charset charset = StandardCharsets.UTF_8;

    public ResponseServiceProvider()
    {
        // default constructor
        this.result = "";
        this.encodeResult = "";
    }

    public String generateResponse(HashSet<KeywordTableRow> set, String[] args) {
        result += ("Content-type: text/html; charset=utf-8\n\n");
        result += ("<!DOCTYPE html>\n");
        result += ("<html>\n");
        result += ("<head>\n");
        result += ("<script>\n");
        result += ("function goBack() {\n");
        result += ("\twindow.location.href = \"/\";\n");
        result += ("}\n");
        result += ("</script>\n");
        result += ("</head>\n\n");
        result += ("<body>\n");
        result += ("<title>Search Engine -" + Arrays.toString(args) + "</title>\n");
        result += ("<p>Hello World!</p>\n");
        result += ("Received query: " + Arrays.toString(args));

        if (result == null) {
            result += (
                    "<p>" + "Result Not found." + "</p>"
            );
        } else {
            result += ("<p>Search Result(s):<p>");
            for (KeywordTableRow keywordTableRow : set) {
                result += (keywordTableRow.toString());
            }
        }

        result += ("<button onclick=\"goBack()\">Go Back</button>\n");
        result += ("</body>\n");
        result += ("</html>\n");

        byte[] raw = Charset.forName("UTF8").encode(result).array();
        encodeResult = new String(raw, charset);

        return encodeResult;
        //return result;
    }
}
