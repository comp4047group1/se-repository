package searchEngine;

import org.jsoup.select.Elements;
import java.util.Date;

/**
 * Created by PIAO Bingjun on 10/1/2016.
 */
public class SearchEngine {
    private Crawler crawler;
    private IOProcessor ioHandler;
    private static int totalKeywordNum;

    /**
     * Create SearchEngine Instance. It is used to build searching index.
     * @param startUrl The start url to crawl
     * @param Y Max number of Processed URL Pool
     * @param X Max number of URL Pool
     */
    public SearchEngine(String startUrl, int Y, int X){
        this.crawler = new Crawler(startUrl, Y , X);
        this.ioHandler = new IOProcessor();
    }

    public void start(){
        long start = new Date().getTime() ;

        HTMLProcessor.setIOProcessor(ioHandler);
        crawler.start();
        ioHandler.closeBuffer();

        long finish = new Date().getTime();

        double timeCost = (finish - start)/1000.0;

        System.out.println("Totally " + totalKeywordNum + " keywords were found!");
        System.out.println("Finish building. Time cost: " + timeCost + " seconds.");
    }

    public static void bridge(String urlForKeyword, Elements links, String title, String plainText){
        HTMLProcessor htmlProcessor = new HTMLProcessor(urlForKeyword, links, title, plainText);
        totalKeywordNum += htmlProcessor.getContent().size();
        htmlProcessor.writeKeyWords();
    }

    public static void main(String[] args) {

        if (0 == args.length) {
            System.exit(-1);
        }

        String startUrl = null;
        int Y = -1;
        int X = -1;

        if (1 <= args.length) {
            startUrl = args[0];
        }
        if (2 <= args.length) {
            Y = Integer.parseInt(args[1]);
        }
        if (3 <= args.length) {
            X = Integer.parseInt(args[2]);
        }

        SearchEngine searchEngine = new SearchEngine(startUrl, Y, X);

        searchEngine.start();
    }
}
