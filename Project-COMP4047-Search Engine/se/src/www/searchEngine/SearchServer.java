package searchEngine;

import java.util.*;
import com.google.gson.*;

/**
 * CGI Test
 * <p/>
 * Created by Jason HO on 8/10/2016.
 */
public class SearchServer {
    private OutputFileReader ofr;
    private SearchServiceProvider ssp;
    private ResponseServiceProvider responseServiceProvider;

    public SearchServer(String path){
        this.ofr = new OutputFileReader(path);
        this.ssp = new SearchServiceProvider(ofr.getHashTable());
        this.responseServiceProvider = new ResponseServiceProvider();
    }

    public static void main(String[] args) {

        SearchServer ss = new SearchServer("out.txt");

        HashSet<KeywordTableRow> result = null;
        if (  args.length > 0 && !args[0].isEmpty() ) {
            result = ss.ssp.search(args);
        }
        
        System.out.print(ss.responseServiceProvider.generateResponse(result, args));

    }


}


