package searchEngine;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

/**
 * Created by Jason HO on 21/10/2016.
 */
public class SearchServiceProvider {
    private Hashtable<String, Hashtable<Integer, HashSet<KeywordTableRow>>> hashTable;

    public SearchServiceProvider( Hashtable<String, Hashtable<Integer, HashSet<KeywordTableRow>>> hashTable) {
        this.hashTable = hashTable;
    }
    /**
     * Perform search function
     * @param keywords args array
     * @return search result set
     */
    public HashSet search(String[] keywords)
    {
        HashSet<KeywordTableRow> result = new HashSet<>();
        Hashtable posToRows = this.hashTable.get(keywords[0].toLowerCase());
        if (posToRows != null && !posToRows.isEmpty()) {
            Set<Integer> keys = posToRows.keySet();

            keys.parallelStream().forEach( key -> {
                HashSet<KeywordTableRow> resultPart = (HashSet<KeywordTableRow>) posToRows.get(key);
                resultPart.retainAll(recursiveSearch(1, (int) key, this.hashTable, resultPart, keywords));
                result.addAll(resultPart);
            });
        }
        return result;
    }

    /**
     * recursively search by looking adj. keywords
     * @param i array index of next keyword in args array
     * @param index Keyword index of previous keyword
     * @param hashTable crawl result
     * @param result search result set
     * @param keywords args array
     * @return search result set
     */
    private HashSet recursiveSearch(Integer i, Integer index, Hashtable<String, Hashtable<Integer, HashSet<KeywordTableRow>>> hashTable
            ,HashSet<KeywordTableRow> result, String[] keywords)
    {
        if (i == keywords.length)
        {
            return result;
        }

        Hashtable posToRows = hashTable.get(keywords[i].toLowerCase());
        int nextIndex = index + 1;
        if (posToRows.containsKey(nextIndex)) {
            HashSet<KeywordTableRow> subResult = (HashSet<KeywordTableRow>) posToRows.get(nextIndex);
            result.retainAll(subResult);
            result.retainAll(recursiveSearch(i+1, nextIndex, hashTable, result, keywords));
        } else {
            result.retainAll( new HashSet<KeywordTableRow>() ); // intersection with empty set to clear unwanted results
            return result;
        }
        return result;
    }
}
