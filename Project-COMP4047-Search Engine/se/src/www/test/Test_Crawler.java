package test;

import org.jsoup.select.Elements;
import searchEngine.*;

/**
 * Created by PIAO Bingjun on 9/30/2016.
 */
public class Test_Crawler {
    public static Elements getLink(){
        String start_url = "http://www.hkbu.edu.hk";
        int X = 3;
        int Y = 3;
        System.out.println("start crawling from " + start_url + " with Y=" + Y + " and X=" + X);
        Crawler crawler = new Crawler(start_url, Y, X);
        crawler.start();
        return null;
    }

    public static void main(String[] args) {

        if (0 == args.length) {
            System.exit(-1);
        }

        String start_url = null;
        int Y = -1;
        int X = -1;

        if (1 <= args.length) {
            start_url = args[0];
        }
        if (2 <= args.length) {
            Y = Integer.parseInt(args[1]);
        }
        if (3 <= args.length) {
            X = Integer.parseInt(args[2]);
        }
        System.out.println("start crawling from " + start_url + " with Y=" + Y + " and X=" + X);
        System.out.println("URL Pool is shown below:");
        Crawler crawler = new Crawler(start_url, Y, X);

        IOProcessor ioHandler = new IOProcessor();
        HTMLProcessor.setIOProcessor(ioHandler);

        crawler.start();

        ioHandler.closeBuffer();
    }
}
