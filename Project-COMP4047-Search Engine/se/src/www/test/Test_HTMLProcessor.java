package test;

import org.jsoup.select.Elements;
import searchEngine.*;

/**
 * Created by PIAO Bingjun on 9/30/2016.
 */
public class Test_HTMLProcessor {
    public static void main(String[] args){
        Elements link = Test_Crawler.getLink();
        //Elements link = null;
        IOProcessor ioHandler = new IOProcessor();

        HTMLProcessor.setIOProcessor(ioHandler);


        HTMLProcessor htmlProcessor = new HTMLProcessor("www.hkbu.edu.hk", link, "HKBU Website", "I have a pen.");

        htmlProcessor.writeKeyWords();

        int size = link.size();
        for(int i = 0; i< size; i++){
            System.out.println(link.get(i).text());
        }
    }
}
