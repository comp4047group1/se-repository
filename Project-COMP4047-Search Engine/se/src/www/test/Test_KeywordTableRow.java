package test;

import searchEngine.Keyword;
import searchEngine.KeywordTableRow;
import com.google.gson.*;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by PIAO Bingjun on 10/19/2016.
 */
public class Test_KeywordTableRow {
    public static void main(String[] args){

        if (false) {
            Keyword keyword1 = new Keyword("yes", 1);
            Keyword keyword2 = new Keyword("no", 2);
            Keyword[] keywords = new Keyword[2];
            keywords[0] = keyword1;
            keywords[1] = keyword2;

            KeywordTableRow row = new KeywordTableRow("HKBU", "www.hkbu.edu.hk", keywords);

            System.out.println(row.createJsonTableRow());

        }



        if(true) {
            Scanner scanner;
            Gson gson = new Gson();
            try {
                Path currPath = Paths.get("out/production/se/out.txt");

                FileInputStream fileInputStream = new FileInputStream(new File(currPath.toAbsolutePath().toString()));
                scanner = new Scanner(new BufferedInputStream(fileInputStream));
                //String[] json = gson.fromJson(scanner.nextLine(), String[].class);
                //System.out.println(json[0]);
                //KeywordTableRow keywordTableRow = new KeywordTableRow(json);

                Hashtable hashtable = new Hashtable<String, HashSet<Keyword>>();

                while (scanner.hasNextLine()) {
                    String[] json = gson.fromJson(scanner.nextLine(), String[].class);
                    KeywordTableRow keywordTableRow = new KeywordTableRow(json);
                    Keyword[] keywords = keywordTableRow.getKeywords();

                    for (int i = 0; i < keywords.length; i++){
                        if (hashtable.containsKey(keywords[i].getKeyword())){
                            Set set = (HashSet<Keyword>) hashtable.get(keywords[i].getKeyword());
                            set.add(keywords[i]);
                        } else {
                            Set set = new HashSet<Keyword>();
                            set.add(keywords[i]);
                            hashtable.put(keywords[i].getKeyword(), set);
                        }
                    }
                }

            } catch (FileNotFoundException fne) {
                System.out.println("File cannot be open or it does not exist");
//            fne.printStackTrace();
                System.exit(1);
            }
        }

    }
}